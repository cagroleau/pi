module Main where

-- main :: IO ()
-- main = putStrLn "Hello, Haskell!"

-- const pi = function(c = count) {
--     var inside = 0;
--
--     for (var i = 0; i < c; i++) {
--         var x = Math.random() * 2 - 1;
--         var y = Math.random() * 2 - 1;
--         if ((x * x + y * y) < 1) {
--             inside++
--         }
--     }
--
--     return 4.0 * inside / c;
-- }

import System.Random

main :: IO ()
-- main = do
--     randomNumber <- randomRIO (1, 100)
-- putStrLn $ "Random number: " ++ show randomNumber
--
threeCoins :: StdGen -> (Bool, Bool, Bool)
threeCoins gen =
  let (firstCoin, newGen) = random gen
      (secondCoin, newGen') = random newGen
      (thirdCoin, newGen'') = random newGen'
   in (firstCoin, secondCoin, thirdCoin)
main = putStrLn threeCoins (mkStdGen 36)
